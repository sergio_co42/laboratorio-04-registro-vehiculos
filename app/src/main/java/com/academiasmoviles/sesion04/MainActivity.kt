package com.academiasmoviles.sesion04

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.academiasmoviles.sesion04.model.Vehiculos
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.alerta_bienvenido.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*var listaInmutable = listOf("Lunes","Martes","Miercoles","Jueves","Viernes")
        println(listaInmutable[2])//Miercoles
        println(listaInmutable.first())//lunes
        println(listaInmutable.last())//viernes
        println(listaInmutable.size)//5 elementos*/

        val toyota: Vehiculos = Vehiculos("Toyota","Corolla",2000)
        val audi: Vehiculos = Vehiculos("Audi","R8",2008)
        val susuki: Vehiculos = Vehiculos("Susuki","Celerio",2018)
        val mazda: Vehiculos = Vehiculos("Mazda","Z200",2015)

        val autos = listOf(audi.Marca,toyota.Marca,susuki.Marca,mazda.Marca)
        val adaptador = ArrayAdapter(this,R.layout.spinneritem,autos)
        spSpinner.adapter= adaptador

        btnAlert.setOnClickListener {
            crearAlerta().show()
        }


    }
    fun crearAlerta (): AlertDialog{
        val alertDialog: AlertDialog
        val builder= AlertDialog.Builder(this)
        //Inflar la vista view= alerta_bienvenido.xml
        val view: View = layoutInflater.inflate(R.layout.alerta_bienvenido,null)
        builder.setView(view)

        val tvTitulo: TextView = view.findViewById(R.id.tvTitulo)
        val btnAceptar: Button = view.findViewById(R.id.btnAceptar)
        val btnCerrar: Button = view.findViewById(R.id.btnCerrar)

        var tvtitular: TextView = view.findViewById(R.id.tvTitular)
        var tvMarca: TextView = view.findViewById(R.id.tvMarca)
        var tvAño: TextView = view.findViewById(R.id.tvAño)
        var tvDescripcion: TextView = view.findViewById(R.id.tvDescripcion)




        
        tvtitular.text= edtTitular.text
        tvMarca.text= spSpinner.selectedItem.toString()
        tvAño.text= edtAño.text
        tvDescripcion.text= edtDescripcion.text

        alertDialog= builder.create()

        btnCerrar.setOnClickListener {
            alertDialog.dismiss()
        }

        btnAceptar.setOnClickListener {
            Toast.makeText(this,"Registrado",Toast.LENGTH_SHORT).show()
        }

        return alertDialog
    }
}