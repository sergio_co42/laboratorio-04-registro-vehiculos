package com.academiasmoviles.sesion04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.academiasmoviles.sesion04.model.Vehiculos

class ObjetosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_objetos)
        val toyota: Vehiculos = Vehiculos("Toyota","Corolla",2000)
        println(toyota.Marca)
        println(toyota.modelo)
        println(toyota.año)
        val audi: Vehiculos = Vehiculos("Audi","R8",2008)
        val susuki: Vehiculos = Vehiculos("Susuki","Celerio",2018)
        val mazda: Vehiculos = Vehiculos("Mazda","Z200",2015)

    }
}