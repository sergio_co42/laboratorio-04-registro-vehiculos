package com.academiasmoviles.sesion04.model

open class Animal(var tipoAnimal:String, var numeroPatas:Int){

    //var tipoAnimal :String = tipoAnimal
    //var numeroPatas :Int = numeroPatas

    //Contructores
    constructor(numeroPatas: Int):this("",numeroPatas)
    constructor(tipoAnimal: String):this(tipoAnimal,0)

    open fun comer(){
        println("El $tipoAnimal que tiene $numeroPatas patas esta comiendo")
    }

    fun dormir(){
        println("El $tipoAnimal que tiene $numeroPatas patas esta durmiendo")
    }

}
