package com.academiasmoviles.sesion04.model

class Perro (val raza:String) : Animal(tipoAnimal = "",numeroPatas = 0){


    constructor () : this("") {
    }

    fun ladrar(){
        println("El perro de raza $raza esta ladrando")
    }

    override fun comer() {
        println("El $tipoAnimal que tiene $numeroPatas patas esta comiendo PROPIO")
    }
}
